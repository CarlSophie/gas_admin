import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true, },
  { path: '/404', component: () => import('@/views/404'), hidden: true },
  {
    path: '/login/sendMsg',
    hidden: true,
    component: () => import('@/views/login/sendMsg')
  },
  {
    path: '/login/setPwd',
    hidden: true,
    component: () => import('@/views/login/setPwd')
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },
  {
    path: '/goods',
    component: Layout,
    meta: { title: '商品管理', icon: 'example' },
    children: [
      {
        path: 'manage',
        component: () => import('@/views/goods/manage/index'),
        meta: { title: '商品管理', icon: '' }
      }
    ]
  },
  {
    path: '/send',
    component: Layout,
    redirect: '/send/account',
    meta: { title: '派送员管理', icon: 'example' },
    children: [
      {
        path: 'account',
        component: () => import('@/views/send/account/index'),
        meta: { title: '创建账号', icon: '' }
      },
      {
        path: 'person',
        component: () => import('@/views/send/person/index'),
        meta: { title: '人员管理', icon: '' }
      }
    ]
  },
  {
    path: '/info',
    component: Layout,
    redirect: '/info/user',
    meta: { title: '信息管理', icon: 'example' },
    children: [
      {
        path: 'user',
        component: () => import('@/views/info/user/index'),
        meta: { title: '用户管理', icon: '' }
      },
      {
        path: 'admin',
        component: () => import('@/views/info/admin/index'),
        meta: { title: '平台管理员', icon: '' }
      }
      // ,
      // {
      //   path: 'role',
      //   component: () => import('@/views/info/role/index'),
      //   meta: { title: '角色管理', icon: 'tree' }
      // }
    ]
  },
  {
    path: '/sendManage',
    component: Layout,
    redirect: '/sendManage/monitor',
    meta: { title: '派送管理', icon: 'example' },
    children: [
      {
        path: 'monitor',
        component: () => import('@/views/sendManage/monitor/index'),
        meta: { title: '派单监控', icon: '' }
      },
      {
        path: 'statistics',
        component: () => import('@/views/sendManage/statistics/index'),
        meta: { title: '派送统计', icon: '' }
      }
      // ,
      // {
      //   path: 'monitor/mapSend',
      //   hidden: true,
      //   component: () => import('@/views/sendManage/monitor/mapSend')
      // }
    ]
  },
  {
    path: '/order',
    component: Layout,
    redirect: '/order/list',
    meta: { title: '订单管理', icon: 'example' },
    children: [
      {
        path: 'list',
        component: () => import('@/views/order/list/index'),
        meta: { title: '订单列表', icon: '' }
      },
      {
        path: 'statistics',
        component: () => import('@/views/order/statistics/index'),
        meta: { title: '订单统计', icon: '' }
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
