import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

const UserKey = 'user'
export function getUser() {
  const data = localStorage.getItem(UserKey)
  if (data != null) {
    return JSON.parse(data)
  } else {
    return null
  }
}

export function setUser(data) {
  const dataStr = JSON.stringify(data)
  return localStorage.setItem(UserKey, dataStr)
}

export function removeUser() {
  return localStorage.removeItem(UserKey)
}
